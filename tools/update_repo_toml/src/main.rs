use std::env;
use std::fs;
use std::path::Path;
use std::fmt::Write;

// TODO: This was written quickly with no regard for safety. Remove all the unwrap()'s

fn main() {
    let mut dirs: Vec<_> = env::args().collect();
    dirs.reverse();
    dirs.pop();

    for dir in dirs {
        let dir = Path::new(&dir);
        println!("{:?}", dir);
        update_repo_toml(dir).unwrap();
        update_pkg_toml(dir).unwrap();
    }
}

fn update_repo_toml(dir: &Path) -> std::io::Result<()> {
    let data = repo_data(dir);
    let repo_file = dir.join("repo.toml");
    let mut new_content: String = String::new();

    writeln!(&mut new_content, "[packages]").unwrap();
    for pkg in data {
        writeln!(&mut new_content, "{} = \"{}\"", pkg.0, pkg.1).unwrap();
    }

    fs::write(repo_file, new_content)?;
    Ok(())
}

fn update_pkg_toml(dir: &Path) -> std::io::Result<()> {

    if dir.is_dir() {
        for entry in fs::read_dir(dir).unwrap() {
            let entry = entry.unwrap();
            if entry.file_type().unwrap().is_dir() {
                let name = entry.file_name().into_string().unwrap();
                let toml = format!("{}.toml", name);
                let path = entry.path().join(Path::new(&toml));
                println!("{:?}", path);
                let mut new_content: String = String::new();

                writeln!(&mut new_content, "versions = [").unwrap();
                for version in fs::read_dir(entry.path()).unwrap() {
                    let x = version.unwrap();
                    if x.file_type().unwrap().is_dir() {
                        writeln!(&mut new_content, "\t\"{}\",", x.file_name().into_string().unwrap()).unwrap();
                    }
                }
                writeln!(&mut new_content, "]").unwrap();
                println!("{}", new_content);
                fs::write(path, new_content)?;
            }       
        }
    } else {
        panic!("Argument wasnt a directory!");
    }

    Ok(())
}

fn repo_data(dir: &Path) -> Vec<(String, String)> {
    let mut res: Vec<(String, String)> = vec![];
    
    if dir.is_dir() {
        for entry in fs::read_dir(dir).unwrap() {
            let entry = entry.unwrap();
            if entry.file_type().unwrap().is_dir() {
                res.push((entry.file_name().into_string().unwrap(), latest_version(&entry.path())));
                println!("pkg: {:?} latest version: {:?}", entry.file_name(), latest_version(&entry.path()));
            }       
        }
    } else {
        panic!("Argument wasnt a directory!");
    }

    res
}

fn latest_version(dir: &Path) -> String {
    let mut vec: Vec<String> = vec![];

    for entry in fs::read_dir(dir).unwrap() {
        let entry = entry.unwrap();
        if entry.file_type().unwrap().is_dir() {
            vec.push(entry.file_name().into_string().unwrap());
        }       
    }
    vec.sort();
    vec.last().unwrap().to_string()
}